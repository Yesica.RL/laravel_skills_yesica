<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    protected $table='modalidades';

    public function participantes()
    {
    	return $this->hasMany('App\Participante');
    }

    public function clasificacion()
    {
    	return $this->hasMany('App\Participante')->orderBy('puntos');
    }
}
