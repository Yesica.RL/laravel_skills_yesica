<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @property string $nombre
 * @property string $apellidos
 * @property string $centro
 * @property string $tutor
 * @property string $fechaNacimiento
 * @property string $imagen
 * @property int $puntos
 * @property int $modalidad_id
 */
class Participante extends Model
{
    //
}
