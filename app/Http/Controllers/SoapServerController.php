<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\libYesica\WSDLDocument;
use Auth;
use SoapServer;

class SoapServerController extends Controller
{
    private $clase="\\App\\SkillWebService";
	private $uri="http://127.0.0.1/unidad/laravel_skills_YESICA/";
	private $servidor="http://127.0.0.1/unidad/laravel_skills_YESICA/public/api";
	private $urlWSDL="http://127.0.0.1/unidad/laravel_skills_YESICA/public/api/wsdl";

	public function getServer()
    {
    	$server=new SoapServer($this->urlWSDL);
    	$server->setClass($this->clase);
    	$server->handle();
    	exit();
    }

    public function getWSDL()
    {
    	$wsdl=new WSDLDocument($this->clase,$this->servidor,$this->uri);
    	$wsdl->formatOutput=true;
    	header('Content-Type: text/xml');
		echo $wsdl->saveXML();
    }
}
