<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Facades\Storage;

class ParticipantesController extends Controller
{
    public function getCrear()
    {
    	$m=Modalidad::all();
    	return view('participantes.crear',array('modalidades'=>$m));
    }
    public function postCrear(Request $request)
    {
    	$participante=new Participante();
		$participante->nombre=$request->nombre;
    	$participante->imagen=$request->imagen->store('','participantes');
    	$participante->apellidos=$request->apellidos;
    	$participante->centro=$request->centro;
    	$participante->tutor=$request->tutor;
    	$participante->fechaNacimiento=$request->fechaNac;
    	$participante->modalidad_id=$request->modalidad;
    	try{
    		$participante->save();
    		return redirect('modalidades')->with('mensaje','participante:'. $participante->nombre .'guardada');
    	}catch(\Illuminate\Database\QueryException $ex){
    		

    	}
    	

    	return redirect('modalidades');
    }

    public function buscar(Request $request)
    {
       $bus=$request->busqueda;
        $p=Participante::select("tutor")->where("tutor","like","%$bus%")->distinct()->pluck("tutor");

        return response()->json($p);  
    }
}
