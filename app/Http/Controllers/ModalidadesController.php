<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Facades\Storage;

class ModalidadesController extends Controller
{
   public function getTodas()
   {
   	$modalidades=Modalidad::all();
   	return view('modalidades.index', array('modalidades'=>$modalidades));
   } 
   public function getMostrar($slug)
   {
   	$m=Modalidad::where('slug',$slug)->first();
   	return view('modalidades.mostrar',array('modalidad'=>$m));
   }

   public function getPuntuar($slug)
   {
   		$modalidad=Modalidad::where('slug',$slug)->first();
   		$p=$modalidad->participantes;
   		foreach ($p as $par) {
   			$nAleatorio=rand(0,100);
   			$par->puntos=$nAleatorio;
   			$par->save();
   		}
   		return redirect('/modalidades/mostrar/'.$slug);
   		
   }

   public function getResetear($slug)
   {
	   	$modalidad=Modalidad::where('slug',$slug)->first();
	   		$p=$modalidad->participantes;
	   		foreach ($p as $par) {
	   			$par->puntos=-1;
	   			$par->save();
	   		}
	   	return redirect('/modalidades/mostrar/'.$slug);
	}
}
