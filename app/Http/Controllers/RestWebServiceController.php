<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Srt;

class RestWebServiceController extends Controller
{
    public function getGanador($slug)
    {
    	$modalidad=Modalidad::where('slug',$slug)->first();
    	$ganador=$modalidad->clasificacion->first();
    	return response()->json($ganador);
    }
}
