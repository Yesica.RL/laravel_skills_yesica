<?php

namespace App;


use App\Participante;
use App\Modalidad;

class SkillWebService 
{
	/**
	 * metodo que devuelve el numero de participantes por centro que quieras
	 * @param string $centro 
	 * @return int
	 */
    public function getNumeroParticipantesCentro($centro)
    {
    	$part= Participante::where('centro',$centro)->get();
    	return count($part);
    }
    /**
     * metodo que devuelve un array de participantes segun el tutor
     * @param string $tutor 
     * @return App\Participante[]
     */
    public function getParticipantesTutor($tutor)
    {
    	return Participante::where('tutor',$tutor)->orderBy("puntos")->get();
    }
}
