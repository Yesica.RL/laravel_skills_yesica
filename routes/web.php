<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ModalidadesController@getTodas');
Route::get('modalidades','ModalidadesController@getTodas');
Route::get('modalidades/mostrar/{slug}','ModalidadesController@getMostrar');
Route::get('modalidades/puntuar/{slug}','ModalidadesController@getPuntuar');
Route::get('modalidades/resetear/{slug}','ModalidadesController@getResetear');
Route::get('participantes/inscribir','ParticipantesController@getCrear');
Route::post('participantes/inscribir','ParticipantesController@postCrear');

Route::any('api', 'SoapServerController@getServer');
Route::any('api/wsdl', 'SoapServerController@getWSDL');

Route::get('rest/ganador/{slug}','RestWebServiceController@getGanador');

Route::post('/busquedaAjax','ParticipantesController@buscar');
