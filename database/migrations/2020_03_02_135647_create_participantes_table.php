<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50);
            $table->string('apellidos',100);
            $table->string('centro');
            $table->string('tutor');
            $table->date('fechaNacimiento');
            $table->string('imagen');
            $table->integer('puntos')->default(-1);
            $table->unsignedBigInteger('modalidad_id');
            $table->timestamps();

            $table->foreign('modalidad_id')->references('id')->on('modalidades');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes');
    }
}
