@extends('layouts.master')
@section('titulo')
	Mostrar Modalidad
@endsection
@section('contenido')
	<div class="col-sm-9">
		<h2>{{$modalidad->nombre}}</h2><br>
		<h3>Familia Profesional:{{$modalidad->familiaProfesional}}</h3>
		<br>
		<h3>Participantes</h3>
		@foreach ($modalidad->participantes as $p)
			<div class="col-xs-12 col-sm-6 col-md-4 ">
				<h4 style="min-height:45px;margin:5px 0 10px 0">
						{{$p->nombre}}
					</h4>
				<img src="{{ asset('assets/imagenes/participantes') }}/{{$p->imagen}}" style="height:200px"/>
			</div>
		@endforeach
		
		<a href="{{url('/modalidades/puntuar/'.$modalidad->slug)}}" class="btn btn-danger">Puntuar</a>
		<a href="{{url('/modalidades/resetear/'.$modalidad->slug)}}" class="btn btn-danger">Resetear</a>
		<table>
			<tr>
				<td>Nombre</td>
				<td>Puntos</td>
			</tr>
			@foreach ($modalidad->clasificacion as $p)
				@if ($p->puntos!=-1)
					<tr>
						<td>{{$p->nombre}}</td>
						<td>{{$p->puntos}}</td>
					</tr>
				@endif
			@endforeach
		</table>
	</div>
@endsection