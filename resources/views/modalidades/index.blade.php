@extends('layouts.master')
@section('titulo')
	Todas Modalidades
@endsection
@section('contenido')
	
	<div class="row">
		@foreach( $modalidades as $m)
			<div class="col-xs-12 col-sm-6 col-md-4 ">
				
					<img src="{{ asset('assets/imagenes/modalidades') }}/{{$m->imagen}}" style="height:200px"/>
				<a href="{{ url('/modalidades/mostrar/'.$m->slug)}}">
					<h4 style="min-height:45px;margin:5px 0 10px 0">
						{{$m->nombre}}
					</h4>
					<h5>{{count($m->participantes)}} Participantes</h5>
				</a>
			</div>
		@endforeach
	</div>
			
@endsection